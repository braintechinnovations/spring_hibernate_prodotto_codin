package com.springhibernate.SpringHibernateProdotto.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springhibernate.SpringHibernateProdotto.models.Prodotto;
import com.springhibernate.SpringHibernateProdotto.repositories.DataAccessRepo;

@Service
public class ProdottoService implements DataAccessRepo<Prodotto>{
 
	@Autowired
	private EntityManager ent_man;
	
	private Session getSessione() {
		return ent_man.unwrap(Session.class);
	}
	
	@Override
	public Prodotto insert(Prodotto t) {

		Prodotto temp = new Prodotto();
		temp.setNome(t.getNome());
		temp.setCodice(t.getCodice());
		temp.setPrezzo(t.getPrezzo());
		
		Session sessione = getSessione();
		
		try {
			sessione.save(temp);
			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		} finally {
			sessione.close();
		}
		
	}

	@Override
	public Prodotto findById(int id) {
		Session sessione = getSessione();
		return (Prodotto) sessione.createCriteria(Prodotto.class)
				.add(Restrictions.eqOrIsNull("id", id))
				.uniqueResult();
		
		//TODO: Trasformarlo in CriteriaQuery
	}

	@Override
	public List<Prodotto> findAll() {
		Session sessione = getSessione();
//		return sessione.createCriteria(Prodotto.class).list();
		
		CriteriaQuery<Prodotto> cq = sessione.getCriteriaBuilder().createQuery(Prodotto.class);
		cq.from(Prodotto.class);
		return sessione.createQuery(cq).list();
	}

	@Transactional
	@Override
	public boolean delete(int id) {

		Session sessione = getSessione();
		
		try {
			
			Prodotto temp = sessione.load(Prodotto.class, id);
			sessione.delete(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
		
	}

	@Transactional
	@Override
	public boolean update(Prodotto t) {

		Session sessione = getSessione();
		
		try {
			Prodotto temp = sessione.load(Prodotto.class, t.getId());
			
			if(t.getNome() != null)
				temp.setNome(t.getNome());
			if(t.getCodice() != null)
				temp.setCodice(t.getCodice());
			if(t.getPrezzo() != null)
				temp.setPrezzo(t.getPrezzo());
			
			sessione.save(temp);
			sessione.flush();
			
			return true;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

}
