package com.springhibernate.SpringHibernateProdotto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springhibernate.SpringHibernateProdotto.models.Prodotto;
import com.springhibernate.SpringHibernateProdotto.services.ProdottoService;

@RestController
@RequestMapping("/prodotto")
public class ProdottoController {

	@Autowired
	private ProdottoService service;
	
	@PostMapping("/inserisci")
	public Prodotto inserisciProdotto(@RequestBody Prodotto objProd) {
		return service.insert(objProd);
	}
	
	@GetMapping("/{prodotto_id}")
	public Prodotto cercaUnicoProdotto(@PathVariable(name="prodotto_id") int prodId) {
		return service.findById(prodId);
	}
	
	@GetMapping("/")
	public List<Prodotto> cercaTuttiProdotti(){
		return service.findAll();
	}
	
	@DeleteMapping("/{prodotto_id}")
	public boolean eliminaProdotto(@PathVariable(name="prodotto_id") int prodId) {
		return service.delete(prodId);
	}
	
	@PutMapping("/update")
	public boolean modificaProdotto(@RequestBody Prodotto objProd) {
		return service.update(objProd);
	}
	
	//TODO: Relazione ManyToMany/OneToMany/OneToOne
	/*
	 * 	C POST http://localhost:8080/studente/insert
		R GET  http://localhost:8080/studente/2
		D DELE http://localhost:8080/studente/2
		U PUT  http://localhost:8080/studente/
		
		C POST http://localhost:8080/esame/insert
		R GET  http://localhost:8080/esame/2
		D DELE http://localhost:8080/esame/2
		U PUT  http://localhost:8080/esame/
		
		C GET  http://localhost:8080/iscrizione/insert/<stud_id>/<esam_id>
	 */
	
}
